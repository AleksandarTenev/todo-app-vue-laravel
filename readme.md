
Getting Started
Make sure you have npm and composer on your local machine

1. Clone this repository and cd into it
2. Run composer install and npm install to download laravel dependencies
3. Run php artisan key:generate to generate a key for the app
4. Set up your pusher account
5. Set up your database in your .env
6. Run php artisan migrate to build your database with tables
7. Execute npm run dev to build project assets
8. Execute php artisan serve to run the project in your browser

Built With
Pusher channels - Pusher Channels
Laravel - Beautiful Php framework
Vue - A Great reactive Js framework
Vuex - Vuejs state management made simple
Bootstrap - A beautiful Css framework
Axios - A Js library to handle ajax requests easily